function problem1(inventory, idToFind) {
    let answer = [];
    if (inventory === undefined || !Array.isArray(inventory) || inventory.length === 0) {
        return answer;
    } else {
        for (let index = 0; index < inventory.length; index++) {
            if (inventory[index].id === idToFind) {
                answer.push(inventory[index]);
                return answer;
            }
        }
        return answer;
    }
}

module.exports = problem1;