function problem2(inventory) {
    let answer = [];
    if (inventory === undefined || !Array.isArray(inventory) || inventory.length === 0) {
        return answer;
    } else {
        answer.push(inventory[inventory.length - 1]);
        return answer;
    }
}

module.exports = problem2;