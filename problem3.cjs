function problem3(inventory) {
    let car_model_names = [];
    if (inventory === undefined || !Array.isArray(inventory) || inventory.length === 0) {
        return car_model_names;
    } else {
        for (let index = 0; index < inventory.length; index++) {
            car_model_names.push(inventory[index].car_model);
        }
        return car_model_names.sort(function (firstVariable, secondVariable) {
            firstVariable = firstVariable.toLowerCase();
            secondVariable = secondVariable.toLowerCase();
            if (firstVariable == secondVariable) return 0;
            if (firstVariable > secondVariable) return 1;
            return -1;
        });
    }
}

module.exports = problem3;