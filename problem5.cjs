function problem5(inventory) {
    let allCars = [];
    if (inventory === undefined || !Array.isArray(inventory) || inventory.length === 0) {
        return allCars;
    } else {
        for (let index = 0; index < inventory.length; index++) {
            if (inventory[index].car_year < 2000) {
                allCars.push(inventory[index].car_make);
            }
        }
        return allCars;
    }
}

module.exports = problem5;