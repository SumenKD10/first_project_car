function problem6(inventory) {
    let carData = [];
    if (inventory === undefined || !Array.isArray(inventory) || inventory.length === 0) {
        return carData;
    } else {
        for (let index = 0; index < inventory.length; index++) {
            if (inventory[index].car_make == "BMW" || inventory[index].car_make == "Audi") {
                carData.push(inventory[index]);
            }
        }
        return carData;
    }
}

module.exports = problem6;